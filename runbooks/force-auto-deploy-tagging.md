# Force auto-deploy tagging

Ordinarily, the auto-deploy tagging procedure requires that a commit on an
auto-deploy branch has a full, successful CI pipeline in order to be considered
a candidate for tagging.

A release manager can override this requirement and force a tagging on the
latest commit regardless of its pipeline status.

A time-sensitive change that can't wait for a full test pipeline to complete, or
a change to QA tests that wouldn't run a full test pipeline are two examples
where this may be called for.

## Procedure

To force a tagging of the latest commit in the current auto-deploy branch,
regardless of its pipeline status:

1. Enable the [`auto_deploy_tag_latest` feature flag][flag].
1. Manually trigger the [`auto_deploy:tag` scheduled job][schedule].
1. Verify [a pipeline is run with the `auto_deploy:tag` job][pipeline], and that
   it tags release-tools, creating a coordinator pipeline with an
   `auto_deploy:start` job.
1. Disable the [feature flag][flag].

Once the latest commit has been tagged, we'll still want to know that it
eventually has a passing CI build. Keep an eye on the commit's pipeline status
to verify it passes or manually [stop deployments][] if it fails.

[flag]: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags/191/edit
[schedule]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[pipeline]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines
[stop deployments]: ../general/deploy/stopping-auto-deploy.md
